
var AWS = require('aws-sdk');
var dynamo = new AWS.DynamoDB.DocumentClient();
var async = require('async');
///Food for thought: we could probably group events together if they go to the same emailmessage to avoid hitting the same document too frequently.
exports.handler = function(mandrillEvents, context, callback) {
	if(!mandrillEvents || mandrillEvents.length <= 0){
		callback(new Error('could not find a valid SNS email message.'))
		return;
	}
	
	var mandrillEvents = mandrillEvents.filter(function(mandrillEvent){
		return mandrillEvent.Environment == process.env.ENVIRONMENT && mandrillEvent.EmailMessageId;
	});

	console.log("number of events: " + mandrillEvents.length);
	var incrementSuccessCounterTasks = CreateIncrementTasks(mandrillEvents);

	//mandrill will send two events in the case of a bounce: first is 'sent' and second is 'bounce'. Therefore we have to undo the counter increment that was initially performed.
	var decrementSuccessCounterTasks = CreateDecrementTasks(mandrillEvents);

	var logTasks = CreateLogBatches(mandrillEvents);

	async.parallel(incrementSuccessCounterTasks.concat(logTasks).concat(decrementSuccessCounterTasks), function(err){
		if(err){
			callback(err);
		} else {
			callback(null);
		}
	})
};

/*
Aggregating update requests into one will reduce overall running time and cost. since
mandrill events usually come in groups of 200 or so anyway.
*/
function CreateDecrementTasks(mandrillEvents){
	var updateSuccessfulCounts = {};
	mandrillEvents.forEach(function(mandrillEvent){
		if(mandrillEvent.MandrillStatus.toLowerCase() != "soft-bounced" && mandrillEvent.MandrillStatus.toLowerCase() != "hard-bounced")
			return;
		if(!updateSuccessfulCounts[mandrillEvent.EmailMessageId])
			updateSuccessfulCounts[mandrillEvent.EmailMessageId] = {
				"EmailMessageId": mandrillEvent.EmailMessageId,
				"EntityId": mandrillEvent.EntityId,
				"Count": 0
			}
		updateSuccessfulCounts[mandrillEvent.EmailMessageId].Count += 1;
	});

	return Object.keys(updateSuccessfulCounts).map(function(counterKey){
		var counterObject = updateSuccessfulCounts[counterKey];
		return function(updateCallback){		
			DecrementSuccessCounter(counterObject.EntityId, counterObject.EmailMessageId, counterObject.Count, updateCallback);
		}
	});
}

/*
Aggregating update requests into one will reduce overall running time and cost. since
mandrill events usually come in groups of 200 or so anyway.
*/
function CreateIncrementTasks(mandrillEvents){
	var updateSuccessfulCounts = {};
	var updateUnsuccessfulCounts = {};
	mandrillEvents.forEach(function(mandrillEvent){
		if(mandrillEvent.MandrillStatus.toLowerCase() == 'sent'){
			if(!updateSuccessfulCounts[mandrillEvent.EmailMessageId])
				updateSuccessfulCounts[mandrillEvent.EmailMessageId] = {
					"EmailMessageId": mandrillEvent.EmailMessageId,
					"EntityId": mandrillEvent.EntityId,
					"Count": 0
				}
			updateSuccessfulCounts[mandrillEvent.EmailMessageId].Count += 1;
		} else {
			if(!updateUnsuccessfulCounts[mandrillEvent.EmailMessageId])
				updateUnsuccessfulCounts[mandrillEvent.EmailMessageId] = {
					"EmailMessageId": mandrillEvent.EmailMessageId,
					"EntityId": mandrillEvent.EntityId,
					"Count": 0
				}
			updateUnsuccessfulCounts[mandrillEvent.EmailMessageId].Count += 1;
		}
	});
	var updateSuccessfulCounterTasks = Object.keys(updateSuccessfulCounts).map(function(counterKey){
		var counterObject = updateSuccessfulCounts[counterKey];
		return function(updateCallback){		
			IncrementSuccessCounter(counterObject.EntityId, counterObject.EmailMessageId, counterObject.Count, true, updateCallback);
		}
	});
	var updateUnsuccessfulCounterTasks = Object.keys(updateUnsuccessfulCounts).map(function(counterKey){
		var counterObject = updateUnsuccessfulCounts[counterKey];
		return function(updateCallback){		
			IncrementSuccessCounter(counterObject.EntityId, counterObject.EmailMessageId, counterObject.Count, false, updateCallback);
		}
	});

	return updateSuccessfulCounterTasks.concat(updateUnsuccessfulCounterTasks);
}

function IncrementSuccessCounter(entityId, emailMessageId, count, isSuccessful, callback){
	console.log("begin increment: " + emailMessageId + " " + count);
	var toUpdate = isSuccessful ? "TotalSuccessful" : "TotalUnSuccessful";
	var params = {
		TableName: "EmailMessage",
		Key: {
			"EntityId": entityId,
			"Id": emailMessageId
		},
		UpdateExpression: "set " + toUpdate + " = " + toUpdate + " + :val",
		ExpressionAttributeValues: {
			":val": count
		},
		ReturnValues: "UPDATED_NEW"
	};

	dynamo.update(params, function(err){
		if(err){
			var error = "error on increment success " + emailMessageId + ":" + err;
			console.log(error);
			callback(err);
		} else {
			console.log("end increment: " + emailMessageId + " " + count);
			callback(null);
		}
	});
}

function DecrementSuccessCounter(entityId, emailMessageId, count, callback){
	console.log("begin decrement: " + emailMessageId + " " + count);
	var params = {
		TableName: "EmailMessage",
		Key: {
			"EntityId": entityId,
			"Id": emailMessageId
		},
		UpdateExpression: "set " + "TotalSuccessful" + " = " + "TotalSuccessful" + " - :val",
		ExpressionAttributeValues: {
			":val": count
		},
		ReturnValues: "UPDATED_NEW"
	};

	dynamo.update(params, function(err){
		if(err){
			var error = "error on decrement success " + emailMessageId + ": " + err;
			console.log(error);
			callback(err);
		} else {
			console.log("end decrement: " + emailMessageId + " " + count);
			callback(null);
		}
	});
}

function CreateLogBatches(mandrillEvents){
	var batchTasks = [];
	var i,j, chunk = 25;
	for(i=0, j=mandrillEvents.length; i<j; i+=chunk){
		(function(){
			batchTasks.push(CreateLogBatch(mandrillEvents.slice(i, i+chunk)));	
		})()
	}

	return batchTasks;
}

function CreateLogBatch(mandrillEvents){
	return function(callback){
		console.log("begin batch log");
		BatchGetLog(mandrillEvents, function(err, emailLogs){
			if(err){
				console.log("error on batch get for log " + ":" +  err);
				callback(err);
				return;
			}

			var params = {
				RequestItems: {
					"EmailLogs": []
				}
			};

			var emailLogDictionary = {};
			emailLogs.Responses["EmailLogs"].forEach((emailLog) =>{
				emailLogDictionary[emailLog.EmailMessageId + emailLog.RecipientUserId] = emailLog;
			});

			mandrillEvents.forEach((mandrillEvent)=>{
				var emailLog = emailLogDictionary[mandrillEvent.EmailMessageId + mandrillEvent.RecipientUserId];
				var item = {
						//key
						"EmailMessageId": mandrillEvent.EmailMessageId,
						"RecipientUserId": mandrillEvent.RecipientUserId,

						//to update
						"MandrillStatus": mandrillEvent.MandrillStatus,
						"EntityId": mandrillEvent.EntityId
					};
				if(emailLog)
					AddPropertyIfDefined(item, emailLog, "FirstName", "LastName", "RecipientEmail", "SystemSendDate", "SendUserId", "EntityId");
				AddPropertyIfDefined(item, mandrillEvent, "MandrillSendRequestDate");
				if(mandrillEvent.ErrorMessage)
					item.ErrorMessage = mandrillEvent.ErrorMessage;
				else
					item.ErrorMessage = mandrillEvent.MandrillStatus;
				params.RequestItems["EmailLogs"].push({
					PutRequest: {
						Item: item
					}
				})
			});

			dynamo.batchWrite(params, function(err){
				if(err)
					console.log("error on batch write for log " + ":" +  err);
				
				callback(null);
			});
		})
	};
}

function AddPropertyIfDefined(addTo, addFrom, ...propertyNames){
	propertyNames.forEach(function(propertyName){
		if(!addFrom[propertyName]) return;
		addTo[propertyName] = addFrom[propertyName];
	})
	
}

//Turns out that performing a joint batch get and batch write is faster and cheaper than individual updates
function BatchGetLog(mandrillEvents, callback){
	var params = {
		"RequestItems": {
			"EmailLogs": {
				"Keys" : mandrillEvents.map((mandrillEvent)=>{
					return {
						"EmailMessageId": mandrillEvent.EmailMessageId,
						"RecipientUserId": mandrillEvent.RecipientUserId
					}
				})
			}
		}
	};

	dynamo.batchGet(params, callback);
}